from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import Http404
from .models import Advertise
from pages.basvuru import data 
import django.utils.timezone as timezone
from user.views import get_querset 
def funcname(parameter_list):
    pass

# Create your views here.
def add_to_db(title1,des,types,sal,dur,dur1,dur2,fund,isp,fls):
    ad1= Advertise.objects.all()
    
    adx=Advertise(title=title1,description=des,typeofposition=types,salary=sal,duration=dur,duration1=dur1,duration2=dur2,fundingagency=fund,isPublished=isp,filess=fls)
    counter=0
    for item in ad1:
        if adx.title == item.title :
            counter+=1
    if counter==0:
        adx.save()        

def index(request):
    
    context={}
    # dict1= data()
    # for key in dict1.keys():
    #     add_to_db(key,dict1[key]["details"],"None",0,'2020-06-12',dict1[key]["start"],dict1[key]["end"],'None',0,None)
    query=''
    if request.GET:
        query=request.GET['q']
        context['query']=str(query)
    
    ad= (get_querset(query))

    context = {
        'advertise': ad
    } 
    return render(request, 'advertise/list.html', context)

def detail(request, advertise_id):
    advertise = get_object_or_404(Advertise, pk = advertise_id)
    context = {
        'advertise': advertise
    }
    return render(request, 'advertise/detail.html', context)

def search(request):
    return render(request, 'advertise/search.html')

