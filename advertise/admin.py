from django.contrib import admin
from .models import Advertise
# Register your models here.
class AdvertiseAdmin(admin.ModelAdmin):
    list_display = ('id','title','isPublished',)
    list_display_links = ('id','title')
    list_filter = ('fundingagency',)
    list_editable = ('isPublished',)
   
    search_fields = ('title','description')
    list_per_page = 20

admin.site.register(Advertise,AdvertiseAdmin)

