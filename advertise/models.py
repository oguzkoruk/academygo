from django.db import models
from django.core.files.storage import FileSystemStorage
from django.forms import ModelForm

# Create your models here.
class Advertise(models.Model):
    ownerofadd=models.CharField(max_length=100,blank=True, null=True)
    title = models.CharField(max_length=100, verbose_name='Title')
    description = models.TextField(verbose_name='Description')
    typeofposition= models.CharField(max_length=50, verbose_name='Type of Position')
    salary = models.CharField(max_length=50,)
    duration= models.DateField(blank=True, null=True)
    duration1=models.DateField(blank=True, null=True)
    duration2=models.DateField(blank=True, null=True)
    
    fundingagency=models.CharField(max_length=100, verbose_name='Funding agency')
    isPublished = models.BooleanField(default= True) 
    filess=models.FileField(blank=True,null=True,upload_to="static/img")

    def __str__(self):
        return self.title
   
