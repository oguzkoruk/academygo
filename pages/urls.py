from django.urls import path
from . import views
from user import views as vw




# http://127.0.0.1:8000/

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('makeanadd',views.makeanadd,name='makeanadd'),
    path('myadds',views.myadds,name='myadds'),
    path('edit',vw.edit,name='edit'),
    
]