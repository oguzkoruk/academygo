from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib import messages
from advertise.models import Advertise 
from django.db.models import Q

# Create your views here.
def get_querset(query=None):
    queryset = []
    queries = query.split(" ")
    for q in queries:
        ads = Advertise.objects.filter(
            Q(title__icontains=q) |
            Q(description__icontains=q)).distinct()
        for ad in ads:
            queryset.append(ad)
    return list(set(queryset))

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password  = request.POST['password']

        user = auth.authenticate(username= username, password = password)
        if user is not None:
            auth.login(request, user)
            messages.add_message(request, messages.SUCCESS,'Oturum açıldı.')
            return redirect('index')
        else:
            messages.add_message(request, messages.ERROR, 'Hatalı username yada parola')
            return redirect('login')
    else:
        return render(request, 'user/login.html')

def register(request):
    if request.method == 'POST':
        
        # get form values
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        repassword = request.POST['repassword']

        if password == repassword:
            # Username
            if User.objects.filter(username = username).exists():
                messages.add_message(request, messages.WARNING, 'Bu kullanıcı adı daha önce alınmış.')
                return redirect('register')
            else:
                if User.objects.filter(email = email).exists():
                    messages.add_message(request, messages.WARNING, 'Bu email daha önce alınmış.')
                    return redirect('register')  
                else:
                    # her şey güzel
                    user = User.objects.create_user(username=username, password= password,email=email)
                    user.save()
                    messages.add_message(request, messages.SUCCESS, 'Hesabınız oluşturuldu.')
                    return redirect('login')
        else:            
            print('parolalar eşleşmiyor')
            return redirect('register')
    else:
        return render(request, 'user/register.html')

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        messages.add_message(request, messages.SUCCESS, 'Oturumunuz kapatıldı.')
        return redirect('index')
def edit(request):
    if request.method == 'POST':
        eusername = request.POST['eusername']
        eemail = request.POST['eemail']
        epassword = request.POST['epassword']
        enewpassword = request.POST['enew-password']
        erenewpassword = request.POST['ere-newpassword']
        current_user = request.user
        
        if current_user.check_password(epassword)==True:
            if enewpassword==erenewpassword:
                current_user.set_password(enewpassword)
                current_user.save()
                messages.add_message(request, messages.SUCCESS, 'Paralonnız değiştirildi.')
                return redirect('index')
            else:
                messages.add_message(request, messages.WARNING,'Parolalar uyuşmuyor')
                return render(request, 'user/edit.html')
        else:
            messages.add_message(request, messages.WARNING, 'parola yanlış.')
            return render(request, 'user/edit.html')
    return render(request, 'user/edit.html')

